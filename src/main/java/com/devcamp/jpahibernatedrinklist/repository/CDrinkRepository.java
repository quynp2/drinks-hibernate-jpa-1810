package com.devcamp.jpahibernatedrinklist.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.jpahibernatedrinklist.model.Drink;

public interface CDrinkRepository extends JpaRepository<Drink, Long> {

}
