package com.devcamp.jpahibernatedrinklist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaHibernateDrinkListApplication {

	public static void main(String[] args) {
		SpringApplication.run(JpaHibernateDrinkListApplication.class, args);
	}

}
